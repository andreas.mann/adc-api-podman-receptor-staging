# Adc Api Podman Staging

adc-api deployment with podman containers.  

(https://gitlab.hzdr.de/scireptor/receptor)

## Installation

Clone the repository to a location where you want to build or pull the\
pod/containers. Create symbolic links to the app and _scriptspod folders:\
ln -s _scriptspod-28ac30ea scriptspod\
ln -s app-28ac30ea app

The configuration file podman-adcapi.conf.sh is configured for these default\
paths: use /var/data as top folder for your deployment.\
/var/data/bkup is the top for the data and bkup files of the running\
container instance.

/var/data/bkup/adc-api-data/conf\
Edit your custom my-custom.cnf file here. It will be added to the mariadb\
database configuration. Also add any sql scripts, which sould run after\
data is imported into a database, e.g. to add views. Each database can\
have its own specific sql file.

/var/data/bkup/adc-adpi-data/sciReptor-dumps\
Add your database dump files here in compressed bz2 format.

### build image or pull an image from container registry
Run the build command in the app folder.\
podman build --tag adc-api-alpine:7d5205ef -f ./Dockerfile\
If you have an image in a remote image registry, configure its name\
in ADCAPI_HTTP_IMAGE, if 'localhost' is not used in the name, image\
will be pulled from the registry.

### update configuration

podman-adcapi.conf.sh

check this configuration file, update values to your requirements, e.g. add\
your databases.  

### install pod

scriptspod/install_adcapi.sh

Run the install script and verify console out, e.g. successfull imports for\
all your databases. 

### remove installation

clean-adc-api.sh

This will remove the running pod and its containers, the created folders on\
the host and the created system service 

### backup workflow

(1) detect_db_change-adcapi.sh

(2) backup_database_adc-api.sh

(3) bkup_file_rotation_adc-api.sh

create a cron job for running (1). If a database table has changed the database\
backup script (2) will run for this database. Create a cron job for (2) to\
regulary create the daily backup folder with name of the date. A cron job\
for (3) will move the created bkup into a folder named with the date. 


### restore a database

Move your backup file to /var/data/bkup/[my folder]/restore. Then run\
restore_database_adc-api.sh [dbname] [dumpfile.sql.bz2] true\
The database must be present in the configuration file. Sql dump file must be\
in bz2 format. True will also apply the custom sql file after import, see\
your configuration file.


### logon to containers

logon-container-http.sh will logon to the http container

logon-container-maria_db.sh will logon to maria db container

logon-container-maria_db-dbconsole.sh will logon to the maria db console

### podman commands

podman pod ps\
shows your running pod

podman inspect pod 1234\
check pod configuration and status of all containers inside the pod

podman ps\
list all containers

podman logs -f 1234\
show console log of container 1234

podman images\
list all your images 

podman rmi 1234\
remove image 1234

### Change in the source code
If you update your source code rebuild the image again, with a new tag\
and specify the new image name in the configuration file. Then run\
the install script again. 

