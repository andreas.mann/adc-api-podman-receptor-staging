#!/bin/bash
# 
# backup db in maria_db container
# ---------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcapi.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE

# check number of arguments
NB_ARGS=1
  if [ $# -lt $NB_ARGS ];
then
	echo "$0: wrong number of arguments ($# instead of at least $NB_ARGS)"
    echo "usage: $0 db_name"
    #echo "e.g. scriptspod/restore_database_adc-api.sh athero_w06_slice1_run4_rebased dump-athero_w06_slice1_run4_rebased-2022-03-10.sql.bz2 true"
	echo "--------------------"
	podman exec -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} --execute='SHOW DATABASES;'"
	echo "--------------------"
    exit 1
fi

# db name
restore_abs_file_path=$1
parent_dir="$(dirname "$restore_abs_file_path")"
base_name="$(basename "$restore_abs_file_path")"

#echo ${DO_BKUP_FLAG}
if [ -f "${DO_BKUP_FLAG}" ]; then
    log "DO_BKUP_FLAG exists...";
    rm ${DO_BKUP_FLAG}
else 
    exit;
fi

#bkup_file_name=mongodb_${host_name}_${POD_NAME_SVC}_$(date +%s).dump
bkup_file_name=mariadb_${host_name}_${ADCAPI_POD_NAME_SVC}_${restore_abs_file_path}_$(date +%Y%m%dT%H%M%S).sql.dump

#sudo docker-compose --file ${SCRIPT_DIR}/docker-compose.yml --project-name turnkey-service exec -T ireceptor-database \
#   sh -c 'mongodump --archive'
# root@98106decdb80:/mnt/bkup# pg_dump -Fc keycloak_db -U postgres -h localhost > /mnt/bkup/incoming/postgres_legolas_adc-keycloak-LEGOLAS-dev_211206134500.sql.dump

podman exec -u root \
            -e bkup_file_name=$bkup_file_name \
            maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
            /bin/bash -c "mysqldump -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} ${restore_abs_file_path}  > /mnt/bkup/incoming/$bkup_file_name"

log "backup db: ${bkup_file_name} created, compressing .."

# compress and remove raw dump
full_path_to_dump="${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/incoming/${bkup_file_name}";
bzip2 -zv  $full_path_to_dump

echo "FIN"
