#!/bin/bash
# detects updated db tables and calls bkup script
#
# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcapi.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE

PARENT_DIR=${SCRIPT_DIR_FULL}/..
CURRENTUSER=$(who | awk 'NR==1{print $1}')
#echo $CURRENTUSER
#echo $SCRIPT_DIR
#echo $SCRIPT_DIR_FULL
#echo $PARENT_DIR

# hostname
host_name=$(hostname);


# -----------------------------------------------------------------------------

log "--- detect db change"
#exit

# Backup storage directory 
backupfolder="/var/data/bkup/mariadb"

# Notification email address 
recipient_email=${sysadmin_email}

# host
# host_name=$(hostname);

# MySQL 
dbuser="root"

# MySQL password
dbpassword=${ADCAPI_MARIADB_ROOT_PASSWORD}

# MySQL host
dbhost="127.0.0.1"

# interval between current time and previous time, for which changed db tables should be detected,1440: in last 24 hrs
db_checked_change_interval_in_mins="1440";

# databases in check
#db_arr=( "employees" "world" "world_x" "sakila" );
#db_arr=${ADCAPI_MARIADB_DB_LIST}

for i in ${ADCAPI_MARIADB_DB_LIST[@]}; do
	echo "db in list: $i ";
done

# Number of days to store the backup 
keep_day=30 

# smb share to copy bkups to 
smb_bkup_dst=""
# smb user with write permission
smb_bkup_user=""
# users passw 
smb_bkup_passw=""
# local mount point for remote share
smb_bkup_local_mount="/mnt/${smb_bkup_dst}"

# sql detect updated tables 
# SELECT table_schema,table_name,update_time FROM information_schema.tables \
# WHERE update_time > (NOW() - INTERVAL 60 MINUTE);

# -----------------------------------------------------------------------------
# get a list of changed tables 

#db_tables_changed=$(mysql -h ${dbhost} -u ${dbuser} -p${dbpassword} \
#	-e "SELECT table_schema,table_name,update_time FROM information_schema.tables \
#	WHERE update_time > (NOW() - INTERVAL ${db_checked_change_interval_in_mins} MINUTE);");

db_tables_changed=$(podman exec -u root \
	-e bkup_file_name=$bkup_file_name \
    maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
    /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e 'SELECT table_schema,table_name,update_time FROM information_schema.tables WHERE update_time > (NOW() - INTERVAL ${db_checked_change_interval_in_mins} MINUTE);'");

#podman exec -u root \
#	-e bkup_file_name=$bkup_file_name \
#    maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
#    /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e 'SELECT table_schema,table_name,update_time FROM information_schema.tables WHERE update_time > (NOW() - INTERVAL ${db_checked_change_interval_in_mins} MINUTE);'"
#exit

#db_tables_changed=$(mysql -h localhost -u root -pPa\$\$w0rd \
#-e "SELECT table_schema,table_name,update_time FROM information_schema.tables \
# WHERE update_time > (NOW() - INTERVAL ${db_checked_change_interval_in_mins} MINUTE);");

echo $db_tables_changed
#exit;
# list of databases, with updated tables 
db_arr_to_bkup=()

# add changed dbs to prev array
for i in ${ADCAPI_MARIADB_DB_LIST[@]}; do
	if [[ ${db_tables_changed} =~ ${i} ]]; then
   		db_arr_to_bkup+=(${i})
	fi
done

log "dbs changed : [ ${db_arr_to_bkup[*]} ] ";
#pwd
#exit
#cd $backupfolder;

for i in ${db_arr_to_bkup[@]}; do
	echo "calling backup for $i"
	touch $DO_BKUP_FLAG
	scriptspod/backup_database_adc-api.sh $i
	
	log "done for $i"
done

#log "delete old backup files..."
#find $backupfolder -mtime +$keep_day -delete

#cd $SCRIPT_DIR_FULL

log "FIN"

exit 0
