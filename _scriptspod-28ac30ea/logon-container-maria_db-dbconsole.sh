#!/bin/bash
SCRIPT_DIR=`dirname "$0"`

SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcapi.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE

podman exec -it --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD}"
