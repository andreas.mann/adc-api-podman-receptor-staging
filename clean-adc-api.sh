#!/bin/bash
echo "all ok: y/n?"
read input
if [ $input != "y"  ]; then
     exit
fi

#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
POD_CONF_FILE="podman-adcapi.conf.sh"
. scriptspod/$POD_CONF_FILE

# stop and disable turnkey service
systemctl --user stop pod-${ADCAPI_POD_NAME_SVC}.service
systemctl --user disable pod-${ADCAPI_POD_NAME_SVC}.service

# stop and remove turnkey-service pod
podman pod stop $ADCAPI_POD_NAME_SVC
sleep 2
podman pod rm $ADCAPI_POD_NAME_SVC
sleep 2

# remove database folder
echo remove db folder
sudo rm -Rf .maria_db_data/
#sudo rm -Rf .maria_db_redis_data/

# check where this file is coming from
rm -f 0
rm -f 1

# remove log
rm -f $LOGFILE_NAME

echo remove bkup paths
# neede for adc-api-test dataset loading
rm -rf ${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/restore
rm -rf ${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/incoming
rm -rf ${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/backup.daily
rm -rf ${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/backup.weekly
rm -rf ${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/backup.monthly
rm -rf ${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}
#export MONGO_DBDIR=/var/data/turnkey-service-php/.mongodb_data

echo remove sql dumps
#rm -f /var/data/bkup/adc-api-data/sciReptor-dumps/*.sql
