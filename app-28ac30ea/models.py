from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Ontology(db.Model):
    __tablename__ = 'ontologies'
    ontology_id = db.Column(
        db.String(100),
        primary_key=True,
        unique=True,
    )
    ontology_label = db.Column(
        db.String(100),
        index=True,
        unique=True,
        nullable=False
    )
    preferred_label = db.Column(
        db.Integer,
        index=True,
        nullable=False
    )


class Receptor(db.Model):
    __tablename__ = 'receptor'
    receptor_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True
    )
    receptor_type = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_1_aa = db.Column(
        db.String(200),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_2_aa = db.Column(
        db.String(200),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_1_locus = db.Column(
        db.String(5),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_2_locus = db.Column(
        db.String(5),
        index=True,
        unique=False,
        nullable=False
    )
    receptor_hash_id = db.Column(
        db.String(200),
        index=True,
        unique=True,
        nullable=True
    )


class Donor(db.Model):
    __tablename__ = 'donor'
    donor_id = db.Column(db.Integer, primary_key=True)
    donor_identifier = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )
    species_id = db.Column(
        db.String(20),
        index=True,
        unique=True,
        nullable=False
    )
    strain = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )
    add_donor_info = db.Column(
        db.String(2000),
        index=True,
        unique=True,
        nullable=False
    )


class Antigen(db.Model):
    __tablename__ = 'antigen'
    antigen_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True
    )
    antigen_type = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    organism = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    organism_curie = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    antigen_source = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    antigen_curie = db.Column(
        index=True,
        unique=False,
        nullable=False
    )
    peptide = db.Column(
        db.String(500),
        index=True,
        unique=False,
        nullable=False
    )
    peptide_start = db.Column(
        db.String(10),
        index=True,
        unique=False,
        nullable=True
    )
    peptide_end = db.Column(
        db.String(10),
        index=True,
        unique=False,
        nullable=True
    )


class Ligand(db.Model):
    __tablename__ = 'ligand'
    ligand_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True
    )
    ligand_type = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=False
    )
    mhc_class = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_1 = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_1_curie = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_allele_1 = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_2 = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_2_curie = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_allele_2 = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    antigen_id = db.Column(
        db.Integer,
        db.ForeignKey('antigen.antigen_id')
    )

    antigen = db.relationship('Antigen', backref='ligand')


class Assay(db.Model):
    __tablename__ = 'assay'
    assay_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True)

    category = db.Column(
        db.String(10),
        index=True,
        unique=False,
        nullable=False
    )
    reactivity_method = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    reactivity_readout = db.Column(
        db.String(45),
        index=True,
        unique=False,
        nullable=False
    )


class Activity(db.Model):
    __tablename__ = 'biological_effector'
    effector_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True
    )
    effector_type = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    activity_item = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )


class Measurement(db.Model):
    __tablename__ = 'measurement'
    node_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True
    )
    assay_id = db.Column(
        db.Integer,
        db.ForeignKey('assay.assay_id')
    )
    receptor_id = db.Column(
        db.Integer,
        db.ForeignKey('receptor.receptor_id')
    )
    ligand_id = db.Column(
        db.Integer,
        db.ForeignKey('ligand.ligand_id')
    )
    effector_id = db.Column(
        db.Integer,
        db.ForeignKey('biological_effector.effector_id')
    )
    reactivity_unit = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=False
    )
    reactivity_value = db.Column(
        db.Float,
        index=True,
        unique=False,
        nullable=False
    )

    assay = db.relationship('Assay', backref='measurement')
    receptor = db.relationship('Receptor', backref='measurement')
    ligand = db.relationship('Ligand', backref='measurement')
    biological_effector = db.relationship('Activity', backref='measurement')


class Receptor_Schema(db.Model):
    __tablename__ = 'reactivity_measurement'
    receptor_id = db.Column(
        db.Integer,
        primary_key=True,
        unique=True
    )
    receptor_type = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_1_aa = db.Column(
        db.String(200),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_2_aa = db.Column(
        db.String(200),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_1_locus = db.Column(
        db.String(5),
        index=True,
        unique=False,
        nullable=False
    )
    variable_domain_2_locus = db.Column(
        db.String(5),
        index=True,
        unique=False,
        nullable=False
    )
    receptor_hash_id = db.Column(
        db.String(200),
        index=True,
        unique=True,
        nullable=True
    )
    ligand_type = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=False
    )
    antigen_type = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    organism = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    organism_curie = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    antigen_source = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    antigen_curie = db.Column(
        index=True,
        unique=False,
        nullable=False
    )
    antigen = db.Column(
        db.String(500),
        index=True,
        unique=False,
        nullable=False
    )
    peptide_start = db.Column(
        db.Integer,
        index=True,
        unique=False,
        nullable=True
    )
    peptide_end = db.Column(
        db.Integer,
        index=True,
        unique=False,
        nullable=True
    )
    mhc_class = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_1 = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_1_curie = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_allele_1 = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_2 = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_gene_2_curie = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    mhc_allele_2 = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=True
    )
    reactivity_method = db.Column(
        db.String(50),
        index=True,
        unique=False,
        nullable=False
    )
    reactivity_readout = db.Column(
        db.String(45),
        index=True,
        unique=False,
        nullable=False
    )
    reactivity_value = db.Column(
        db.Float,
        index=True,
        unique=False,
        nullable=False
    )
    reactivity_unit = db.Column(
        db.String(20),
        index=True,
        unique=False,
        nullable=False
    )