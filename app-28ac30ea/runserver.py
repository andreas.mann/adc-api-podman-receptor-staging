from flask import Flask, request
from views import *
from filter import *
from models import *


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://devel_receptor_new:buRts167FhsK@127.0.0.1/devel_receptor_new'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:******@localhost/final_12_08'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

@app.route('/', methods=['GET', 'POST'])
def get_welcome():
    return {
        "result": "Success",
        "message": "Welcome to sciReptor Server"
    }


@app.route('/info', methods=['GET', 'POST'])
def get_info():
    return {
        "title": "airr-api-ireceptor",
        "description": "AIRR Data Commons API for iReceptor",
        "version": "3.0",
        "last_update": "05.23.2022",
        "contact": {
            "name": "iReceptor",
            "url": "http://www.ireceptor.org",
            "email": "support@ireceptor.org"
        }
    }


@app.route('/receptor', methods=['GET', 'POST'])
def post_receptor():
    receptor_query = db.session.query(
        Receptor.receptor_id,
        Receptor.receptor_type,
        Receptor.variable_domain_1_aa,
        Receptor.variable_domain_2_aa,
        Receptor.variable_domain_1_locus,
        Receptor.variable_domain_2_locus,
        Receptor.receptor_hash_id,
        Ligand.ligand_type,
        Antigen.antigen_type,
        Antigen.antigen_curie,
        Antigen.antigen_source,
        Antigen.organism,
        Antigen.organism_curie,
        Antigen.peptide,  # antigen
        Antigen.peptide_start,
        Antigen.peptide_end,
        Ligand.mhc_class,
        Ligand.mhc_gene_1,
        Ligand.mhc_gene_1_curie,
        Ligand.mhc_allele_1,
        Ligand.mhc_gene_2,
        Ligand.mhc_gene_2_curie,
        Ligand.mhc_allele_2,
        Assay.reactivity_method,
        Assay.reactivity_readout,
        Measurement.reactivity_value,
        Measurement.reactivity_unit
    ).join(Ligand, Ligand.ligand_id == Measurement.ligand_id) \
        .join(Assay, Assay.assay_id == Measurement.assay_id) \
        .join(Antigen, Antigen.antigen_id == Ligand.antigen_id) \
        .filter(Receptor.receptor_id == Measurement.receptor_id)
    return create_response_receptor(receptor_query, request.get_json() if request.data else {})


@app.route('/receptor/<int:receptor_id>', methods=['GET'])
def get_receptor(receptor_id):
    receptor_query = db.session.query(
        Receptor.receptor_id,
        Receptor.receptor_type,
        Receptor.variable_domain_1_aa,
        Receptor.variable_domain_2_aa,
        Receptor.variable_domain_1_locus,
        Receptor.variable_domain_2_locus,
        Receptor.receptor_hash_id,
        Ligand.ligand_type,
        Antigen.antigen_type,
        Antigen.antigen_curie,
        Antigen.antigen_source,
        Antigen.organism,
        Antigen.organism_curie,
        Antigen.peptide,  # antigen
        Antigen.peptide_start,
        Antigen.peptide_end,
        Ligand.mhc_class,
        Ligand.mhc_gene_1,
        Ligand.mhc_gene_1_curie,
        Ligand.mhc_allele_1,
        Ligand.mhc_gene_2,
        Ligand.mhc_gene_2_curie,
        Ligand.mhc_allele_2,
        Assay.reactivity_method,
        Assay.reactivity_readout,
        Measurement.reactivity_value,
        Measurement.reactivity_unit
    ).join(Ligand, Ligand.ligand_id == Measurement.ligand_id) \
        .join(Assay, Assay.assay_id == Measurement.assay_id) \
        .join(Antigen, Antigen.antigen_id == Ligand.antigen_id) \
        .filter(Receptor.receptor_id == Measurement.receptor_id) \
        .filter(Receptor.receptor_id == receptor_id)
    return create_get_receptor(receptor_query)


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)
