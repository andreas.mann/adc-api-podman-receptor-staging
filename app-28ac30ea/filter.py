from argparse import ArgumentError


# adapted from post-query approach for /repertoire endpoint
def filter_results(results, filters):
    filter_func = __build_filter_func(filters)
    results = [result for result in results if filter_func(result)]
    return results

def __build_filter_func(filters):
    op = filters.get('op', None)
    if op in ['and', 'or']:
        conditions = []
        for operand in filters['content']:
            conditions.append(__build_filter_func(operand))
            print(conditions)
        def func(row):
            for condition in conditions:
                if op == 'and':
                    if not condition(row): return False
                else:
                    if condition(row): return True
            return op == 'and'
    else:
        field = filters['content']['field']
        value = filters['content'].get('value', None)
        def func(row):

            return __check_condition(op, QueryDict(row).get(field), value)

    return func


def __check_condition(op, value1, value2):
    operation = __build_operator(op)
    if isinstance(value1, list):
        if op in ['=', '>', '>=', '<', '<=', '!=', 'contains']:
            for v in value1:
                if operation(v, value2):
                    return True
            return False
        elif op in ['is', 'not', 'is missing', 'is not missing']:
            for v in value1:
                if not operation(v, value2):
                    return False
            return True
    # not a list or unsupported operators, including is and not:
    return operation(value1, value2)


def __build_operator(op):
    if op == '=':
        operation = lambda v1, v2: (float(v1) == float(v2)) if str(v1).isnumeric() else (v1 == v2)
    elif op == '!=':
        operation = lambda v1, v2: (float(v1) != float(v2)) if str(v1).isnumeric() else (v1 != v2)
    elif op == '<':
        operation = lambda v1, v2: (float(v1) < float(v2)) if str(v1).isnumeric() else (v1 < v2)
    elif op == '>':
        operation = lambda v1, v2: (float(v1) > float(v2)) if str(v1).isnumeric() else (v1 > v2)
    elif op == '<=':
        operation = lambda v1, v2: (float(v1) <= float(v2)) if str(v1).isnumeric() else (v1 <= v2)
    elif op == '>=':
        operation = lambda v1, v2: (float(v1) >= float(v2)) if str(v1).isnumeric() else (v1 >= v2)
    elif op == 'like':
        operation = lambda v1, v2: v1.like(v2)  # not applied to /receptor endpoint, not solved
    elif op == 'not':
        operation = lambda v1, v2: v1 != v2
    elif op == 'in':
        operation = lambda v1, v2: v1 in v2
    elif op == 'is':
        operation = lambda v1, v2: v1 == v2
    elif op == 'is missing':
        operation = lambda v1, v2: v1 == None or v1 == [None]
    elif op == 'is not missing':
        operation = lambda v1, v2: v1 != None and v1 != [None]
    elif op == 'contains':
        operation = lambda v1, v2: v2 in v1
    elif op == 'exclude':
        operation = lambda v1, v2: v1 not in v2
    elif op == 'and':
        operation = lambda v1, v2: v1 and v2
    elif op == 'or':
        operation = lambda v1, v2: v1 or v2
    else:
        raise ArgumentError()
    return operation


class QueryDict(dict):

    def get(self, path, default=None):
        keys = path.split('.')
        value = None
        for key in keys:
            if value:
                if isinstance(value, list):
                    

                    new_value = []
                    for element in value:
                        if isinstance(element, list):  # only 2 levels of nested lists supported (for now)
                            new_value += [v.get(key, default) if v else None for v in element]
                        elif element:
                            new_value.append(element.get(key, default))
                    value = new_value
                else:
                    value = value.get(key, default)
            else:
                value = dict.get(self, key, default)
    
            if not value:
                break
        return value
