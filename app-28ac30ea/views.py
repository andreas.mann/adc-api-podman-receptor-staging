from flask import jsonify, request
from contextlib import suppress
from models import *
from filter import *
import flatdict
import re
from collections import Counter

info = {
      "title": "airr-api-ireceptor",
      "description": "AIRR Data Commons API for iReceptor",
      "version": "3.0",
      "last_update": None,
      "contact": {
          "name": "iReceptor",
          "url": "http://www.ireceptor.org",
          "email": "support@ireceptor.org"

        }
}

def create_response_receptor(receptors, request_json):
    receptors_ls = []
    for receptor in receptors:
        groups = {
            'receptor_id': receptor[0],
            'receptor_hash': receptor[6],
            'receptor_type': receptor[1],
            'receptor_variable_domain_1_aa': receptor[2],
            'receptor_variable_domain_1_locus': receptor[4],
            'receptor_variable_domain_2_aa': receptor[3],
            'receptor_variable_domain_2_locus': receptor[5],
            'reactivity_measurement': {
                'ligand_type': receptor[7],
                'antigen_type': receptor[8],
                'antigen': {
                    'id': receptor[9],
                    'label': receptor[10],
                },
                'antigen_source_species':{
                    'id': receptor[12],
                    'label': receptor[11],
                },
                'peptide': receptor[13],
                'peptide_start': receptor[14],
                'peptide_end': receptor[15],
                'mhc_class': receptor[16],
                'mhc_gene_1': {
                    'id': receptor[18],
                    'label': receptor[17],
                },
                'mhc_allele_1': receptor[19],
                'mhc_gene_2': {
                    'id': receptor[21],
                    'label': receptor[20],
                },
                'mhc_allele_2': receptor[22],
                'reactivity_method': receptor[23],
                'reactivity_readout': receptor[24],
                'reactivity_value': receptor[25],
                'reactivity_unit': receptor[26]
            }
        }
        receptors_ls.append(groups)

    if 'filters' in request_json:
        receptors_ls = filter_results(receptors_ls, request_json['filters'])

    receptor_ls = []
    for index in set(item['receptor_id'] for item in receptors_ls):
        receptor_type = [
            item['receptor_type'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        type_unique = ''.join(list(set(receptor_type)))
        receptor_variable_domain_1_aa = [
            item['receptor_variable_domain_1_aa'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_1_aa_unique = ''.join(list(set(receptor_variable_domain_1_aa)))
        receptor_variable_domain_2_aa = [
            item['receptor_variable_domain_2_aa'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_2_aa_unique = ''.join(list(set(receptor_variable_domain_2_aa)))
        receptor_variable_domain_1_locus = [
            item['receptor_variable_domain_1_locus'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_1_locus_unique = ''.join(list(set(receptor_variable_domain_1_locus)))
        receptor_variable_domain_2_locus = [
            item['receptor_variable_domain_2_locus'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_2_locus_unique = ''.join(list(set(receptor_variable_domain_2_locus)))
        receptor_hash = [
            item['receptor_hash'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_hash_unique = ''.join(list(set(receptor_hash)))
        reactivity_measurement = [
            item['reactivity_measurement'] for item in receptors_ls
            if item['receptor_id'] == index
        ]

        sub = {
            'receptor_id': index,
            'receptor_type': type_unique,
            'receptor_variable_domain_1_aa': receptor_variable_domain_1_aa_unique,
            'receptor_variable_domain_2_aa': receptor_variable_domain_2_aa_unique,
            'receptor_variable_domain_1_locus': receptor_variable_domain_1_locus_unique,
            'receptor_variable_domain_2_locus': receptor_variable_domain_2_locus_unique,
            'receptor_hash': receptor_hash_unique,
            'reactivity_measurement': reactivity_measurement
        }
        receptor_ls.append(sub)

    if 'facets' in request_json:
        facets = __calculate_facets(receptor_ls, request_json['facets'])
        return jsonify({'Info': info, 'Facet': facets})
    else:
        from_ = int(request_json.get('from', 0))
        size = int(request_json.get('size', len(receptor_ls)))

        if from_ > 0 or size < len(receptor_ls):
            receptor_ls = receptor_ls[from_:from_ + size]

        if 'fields' in request_json:
            display_fields = request_json['fields']
            __filter_fields_with_whitelist(receptor_ls, display_fields)

        return jsonify({'Info': info, 'Receptor': receptor_ls})


def __filter_fields_with_whitelist(obj, whitelist, current_path=''):
    if isinstance(obj, dict):
        current_dict = obj.copy()
        for key, value in current_dict.items():
            elem_path = f'{current_path}.{key}'.lstrip('.')
            # print(elem_path)
            if not any(entry.startswith(elem_path) for entry in whitelist):
                with suppress(KeyError):
                    del obj[key]
            elif isinstance(value, (dict, list)) and not any(elem_path.startswith(entry) for entry in whitelist):
                __filter_fields_with_whitelist(obj[key], whitelist, elem_path)
    elif isinstance(obj, list):
        for elem in obj:
            if isinstance(elem, (dict, list)):
                __filter_fields_with_whitelist(elem, whitelist, current_path)

def __calculate_facets(dicts, facet_field):
    facet_values = []
    for objdict in dicts:
        d = flatdict.FlatterDict(objdict, delimiter='.')
        flatitems = d.items()
        for key, value in flatitems:
            key = re.sub('\.\d+', '', key)
            if key == facet_field:
                facet_values.append(value)
    counts = Counter(facet_values)
    facets = [{facet_field: k, "count": v} for k, v in counts.items()]

    return facets


def create_get_receptor(receptors):
    receptors_ls = []
    for receptor in receptors:
        groups = {
            'receptor_id': receptor[0],
            'receptor_hash': receptor[6],
            'receptor_type': receptor[1],
            'receptor_variable_domain_1_aa': receptor[2],
            'receptor_variable_domain_1_locus': receptor[4],
            'receptor_variable_domain_2_aa': receptor[3],
            'receptor_variable_domain_2_locus': receptor[5],
            'reactivity_measurement': {
                'ligand_type': receptor[7],
                'antigen_type': receptor[8],
                'antigen': {
                    'id': receptor[9],
                    'label': receptor[10],
                },
                'antigen_source_species':{
                    'id': receptor[12],
                    'label': receptor[11],
                },
                'peptide': receptor[13],
                'peptide_start': receptor[14],
                'peptide_end': receptor[15],
                'mhc_class': receptor[16],
                'mhc_gene_1': {
                    'id': receptor[18],
                    'label': receptor[17],
                },
                'mhc_allele_1': receptor[19],
                'mhc_gene_2': {
                    'id': receptor[21],
                    'label': receptor[20],
                },
                'mhc_allele_2': receptor[22],
                'reactivity_method': receptor[23],
                'reactivity_readout': receptor[24],
                'reactivity_value': receptor[25],
                'reactivity_unit': receptor[26]
            }
        }
        receptors_ls.append(groups)

    receptor_ls = []
    for index in set(item['receptor_id'] for item in receptors_ls):
        receptor_type = [
            item['receptor_type'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        type_unique = ''.join(list(set(receptor_type)))
        receptor_variable_domain_1_aa = [
            item['receptor_variable_domain_1_aa'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_1_aa_unique = ''.join(list(set(receptor_variable_domain_1_aa)))
        receptor_variable_domain_2_aa = [
            item['receptor_variable_domain_2_aa'] for  item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_2_aa_unique = ''.join(list(set(receptor_variable_domain_2_aa)))
        receptor_variable_domain_1_locus = [
            item['receptor_variable_domain_1_locus'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_1_locus_unique = ''.join(list(set(receptor_variable_domain_1_locus)))
        receptor_variable_domain_2_locus = [
            item['receptor_variable_domain_2_locus'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_variable_domain_2_locus_unique = ''.join(list(set(receptor_variable_domain_2_locus)))
        receptor_hash = [
            item['receptor_hash'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        receptor_hash_unique = ''.join(list(set(receptor_hash)))
        reactivity_measurement = [
            item['reactivity_measurement'] for item in receptors_ls
            if item['receptor_id'] == index
        ]
        sub = {
            'receptor_id': index,
            'receptor_type': type_unique,
            'receptor_variable_domain_1_aa': receptor_variable_domain_1_aa_unique,
            'receptor_variable_domain_2_aa': receptor_variable_domain_2_aa_unique,
            'receptor_variable_domain_1_locus': receptor_variable_domain_1_locus_unique,
            'receptor_variable_domain_2_locus': receptor_variable_domain_2_locus_unique,
            'receptor_hash': receptor_hash_unique,
            'reactivity_measurement': reactivity_measurement
        }
        receptor_ls.append(sub)
    return jsonify({'Info': info, 'Receptor': receptor_ls})